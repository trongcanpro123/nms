const NodeMediaServer = require('node-media-server');

const config = {
    rtmp: {
        port: 2002,
        chunk_size: 60000,
        gop_cache: true,
        ping: 30,
        ping_timeout: 60
    },
    http: {
        port: 2003,
        allow_origin: '*',
	allow_private_network: true,
    },
    relay: {
        ffmpeg: '/var/www/html/nms/ffmpeg-4.4-amd64-static/ffmpeg',
        tasks: [
            {
                app: 'iptv',
                mode: 'static',
                name: 'cannt',
                edge: 'rtsp://admin:123456a@@10.8.6.136:554/Streaming/Channels/101',
                rtsp_transport: 'tcp'
            },
	        {
                app: 'iptv',
                mode: 'static',
                name: 'cannt_1',
                edge: 'rtsp://admin:123456a@@10.8.6.136:554/Streaming/Channels/101',
                rtsp_transport: 'tcp'
            }
        ]
    }
};

var nms = new NodeMediaServer(config)
nms.run();















































